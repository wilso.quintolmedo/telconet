package com.woqg.telconet.bean;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class GenericResponse<T> {

	String code;
	String message;
	String description;
	@JsonInclude(value = Include.NON_NULL)
	T response;

	public GenericResponse<T> success() {
		init("Succesful Transaction", "200", null, null);
		return this;
	}

	public GenericResponse<T> success(String message) {
		init(message, "200", null, null);
		return this;
	}

	public GenericResponse<T> success(T response) {
		init("Sucessful Response", "200", null, response);
		return this;
	}

	private void init(String code, String message, String description, T response) {
		this.code = code;
		if (message != null)
			this.message = message;
		if (description != null)
			this.description = description;
		if (response != null)
			this.response = response;
	}

	public GenericResponse<T> error(String message, HttpStatus httpStatus) {
		init(httpStatus.value() + "", message, httpStatus.getReasonPhrase(), null);
		return this;
	}
}