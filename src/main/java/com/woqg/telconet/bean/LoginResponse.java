package com.woqg.telconet.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(value = Include.NON_EMPTY)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse {

	private String email;
	private String nombres;
	private String apellidos;
	private List<String> roles;
	private String accessToken;
    private String refreshToken;
    private String tokenType = "Bearer";
    private Long expiryDuration;
}
