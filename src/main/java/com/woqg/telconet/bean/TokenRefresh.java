package com.woqg.telconet.bean;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TokenRefresh {

	@NotBlank(message = "Refresh token cannot be blank")
	private String refreshToken;
}
