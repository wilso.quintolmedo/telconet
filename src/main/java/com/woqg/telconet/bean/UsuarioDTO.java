package com.woqg.telconet.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(value = Include.NON_EMPTY)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDTO {

	private String email;
	private String password;
	private String nombres;
	private String apellidos;
	private String direccion;
	private String telefono;
	private String foto;
	private String rol;
	
	public UsuarioDTO(String email, String password) {
		this.email = email;
		this.password = password;
	}
	
	

}
