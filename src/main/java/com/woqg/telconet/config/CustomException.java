package com.woqg.telconet.config;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CustomException extends Exception {

	private static final long serialVersionUID = 1L;

	private final HttpStatus httpStatus;
	private final String message;

	public CustomException(String message) {
		this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		this.message = message;
	}

	public CustomException(HttpStatus httpStatus, String message) {
		this.httpStatus = httpStatus;
		this.message = message;
	}

}
