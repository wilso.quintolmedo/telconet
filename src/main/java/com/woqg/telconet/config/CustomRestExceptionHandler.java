package com.woqg.telconet.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.woqg.telconet.bean.GenericResponse;

import lombok.extern.log4j.Log4j2;

@Log4j2
@ControllerAdvice
public class CustomRestExceptionHandler {

	@ResponseStatus(HttpStatus.BAD_REQUEST) // 400
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	public GenericResponse<Object> handleIllegal(IllegalArgumentException e) {
		log.error(e.getLocalizedMessage());
		return new GenericResponse<Object>().error(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST) // 400
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseBody
	public GenericResponse<Object> handleConstraintViolationException(ConstraintViolationException e) {
		List<String> errors = new ArrayList<>();
		for (ConstraintViolation<?> violation : e.getConstraintViolations()) {
			errors.add(violation.getMessage());
		}
		String error = errors.toString().replace("[", "").replace("]", "");
		log.error(error);
		return new GenericResponse<Object>().error(error, HttpStatus.BAD_REQUEST);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST) // 400
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseBody
	public GenericResponse<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e) {
		List<String> errors = new ArrayList<>();
		for (FieldError fieldError : e.getBindingResult().getFieldErrors()) {
			errors.add(fieldError.getDefaultMessage());
		}
		String error = errors.toString().replace("[", "").replace("]", "");
		log.error(error);
		return new GenericResponse<Object>().error(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(CustomException.class)
	@ResponseBody
	public ResponseEntity<GenericResponse<Object>> handleIntegrationException(CustomException e, Locale locale) {
		GenericResponse<Object> response = new GenericResponse<>().error(e.getMessage(), e.getHttpStatus());
		log.error(e.getMessage());
		return new ResponseEntity<>(response, e.getHttpStatus());
	}

	@ResponseStatus(HttpStatus.FORBIDDEN) // 403
	@ExceptionHandler(AccessDeniedException.class)
	@ResponseBody
	public GenericResponse<Object> handleOtherException(AccessDeniedException e) {
		GenericResponse<Object> response = new GenericResponse<>().error(e.getMessage(), HttpStatus.FORBIDDEN);
		log.error(e.getMessage());
		return response;
	}

	@ResponseStatus(HttpStatus.UNAUTHORIZED) // 401
	@ExceptionHandler(AuthenticationException.class)
	@ResponseBody
	public GenericResponse<Object> handleOtherException(AuthenticationException e) {
		GenericResponse<Object> response = new GenericResponse<>().error(e.getMessage(), HttpStatus.UNAUTHORIZED);
		log.error(e.getMessage());
		return response;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) // 500
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public GenericResponse<Object> handleOtherException(Exception e) {
		log.error(e.getMessage());
		return new GenericResponse<Object>().error(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
