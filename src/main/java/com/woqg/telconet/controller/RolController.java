package com.woqg.telconet.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.woqg.telconet.bean.RolDTO;
import com.woqg.telconet.config.CustomException;
import com.woqg.telconet.security.service.RolService;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@CrossOrigin(origins = "*")
@RestController
@SecurityRequirement(name = "basicapi")
@PreAuthorize("hasAuthority('ADMINISTRADOR')")
@RequestMapping("/api/rol")
public class RolController {

	@Autowired
	RolService rolSer;

	@GetMapping
	public List<RolDTO> obtener() {
		return rolSer.obtener();
	}

	@GetMapping("/{id}")
	public RolDTO obtenerPorId(@PathParam(value = "id") Long id) throws CustomException {
		return rolSer.obtenerPorId(id);
	}

	@PostMapping
	public RolDTO crear(@Valid @NotNull @RequestBody RolDTO rolDTO) {
		return rolSer.crear(rolDTO);
	}

	@PutMapping
	public RolDTO actualizar(@PathParam(value = "id") Long id, @Valid @NotNull @RequestBody RolDTO rolDTO) throws CustomException {
		return rolSer.actualizar(id, rolDTO);
	}

	@DeleteMapping
	public RolDTO eliminar(@PathParam(value = "id") Long id) throws CustomException {
		return rolSer.eliminar(id);
	}
}
