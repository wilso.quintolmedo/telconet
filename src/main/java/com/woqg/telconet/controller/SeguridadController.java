package com.woqg.telconet.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.woqg.telconet.bean.LoginDTO;
import com.woqg.telconet.bean.LoginResponse;
import com.woqg.telconet.bean.TokenRefresh;
import com.woqg.telconet.config.CustomException;
import com.woqg.telconet.security.service.SeguridadService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/seguridad")
public class SeguridadController {

	@Autowired
	SeguridadService seguridadService;

	@PostMapping("/login")
	public ResponseEntity<LoginResponse> autenticaUsuario(@RequestBody LoginDTO loginDTO) {
		return ResponseEntity.ok(seguridadService.autenticaUsuario(loginDTO));
	}

	@PostMapping("/refreshToken")
	public ResponseEntity<LoginResponse> autenticaUsuario(@RequestBody TokenRefresh tokenRefresh)
			throws CustomException {
		return ResponseEntity.ok(seguridadService.refreshJwtToken(tokenRefresh));
	}

	@DeleteMapping("/logout")
	public ResponseEntity<Void> logoutUsuario(HttpServletRequest request) throws CustomException {
		seguridadService.logoutUsuario(request.getHeader("Authorization"));
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}
}
