package com.woqg.telconet.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.woqg.telconet.bean.UsuarioDTO;
import com.woqg.telconet.config.CustomException;
import com.woqg.telconet.security.service.UsuarioService;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@CrossOrigin(origins = "*")
@RestController
@SecurityRequirement(name = "basicapi")
@RequestMapping("/api/usuario")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioSer;

	@GetMapping
	@PreAuthorize("hasAnyAuthority('CONSULTOR', 'EDITOR', 'ADMINISTRADOR')")
	public List<UsuarioDTO> obtener() {
		return usuarioSer.obtener();
	}
	
	@GetMapping("/email/{email}")
	@PreAuthorize("hasAnyAuthority('CONSULTOR', 'EDITOR', 'ADMINISTRADOR')")
	public List<UsuarioDTO> obtenerPorId(@PathParam(value = "email") String email) throws CustomException {
		return usuarioSer.obtenerEmail(email);
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasAnyAuthority('CONSULTOR', 'EDITOR', 'ADMINISTRADOR')")
	public UsuarioDTO obtenerPorId(@PathParam(value = "id") Long id) throws CustomException {
		return usuarioSer.obtenerPorId(id);
	}

	@PostMapping("/crearUsuario")
	@PreAuthorize("hasAnyAuthority('EDITOR', 'ADMINISTRADOR')")
	public ResponseEntity<Void> crearUsuario(@Valid @NotNull @RequestBody UsuarioDTO usuarioDTO)
			throws CustomException {
		usuarioSer.crear(usuarioDTO);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping
	@PreAuthorize("hasAnyAuthority('EDITOR', 'ADMINISTRADOR')")
	public UsuarioDTO actualizar(@PathParam(value = "id") Long id, @Valid @NotNull @RequestBody UsuarioDTO usuarioDTO)
			throws CustomException {
		return usuarioSer.actualizar(id, usuarioDTO);
	}

	@DeleteMapping
	@PreAuthorize("hasAnyAuthority('EDITOR', 'ADMINISTRADOR')")
	public UsuarioDTO eliminar(@PathParam(value = "id") Long id) throws CustomException {
		return usuarioSer.eliminar(id);
	}

}
