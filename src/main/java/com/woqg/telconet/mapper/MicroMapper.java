package com.woqg.telconet.mapper;

import java.util.Date;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.woqg.telconet.bean.LoginResponse;
import com.woqg.telconet.bean.RolDTO;
import com.woqg.telconet.bean.UsuarioDTO;
import com.woqg.telconet.repositoy.entity.RefreshToken;
import com.woqg.telconet.repositoy.entity.Rol;
import com.woqg.telconet.repositoy.entity.Usuario;

@Mapper
public interface MicroMapper {

	MicroMapper INSTANCE = Mappers.getMapper(MicroMapper.class);

	@Mapping(target = "email", source = "usuario.email")
	@Mapping(target = "nombres", source = "usuario.nombres")
	@Mapping(target = "apellidos", source = "usuario.apellidos")
	@Mapping(target = "roles", source = "roles")
	@Mapping(target = "accessToken", source = "refreshToken.token")
	@Mapping(target = "refreshToken", source = "refreshToken.refToken")
	@Mapping(target = "expiryDuration", source = "expiryDuration")
	LoginResponse entityToDto(Usuario usuario, List<String> roles, RefreshToken refreshToken, int expiryDuration);

	@Mapping(target = "accessToken", source = "refreshToken.token")
	@Mapping(target = "refreshToken", source = "refreshToken.refToken")
	@Mapping(target = "expiryDuration", source = "expiryDuration")
	LoginResponse entityToDto(RefreshToken refreshToken, int expiryDuration);

	Rol dtoToEntity(RolDTO rolDTO);

	RolDTO entityToDto(Rol rol);
	
	UsuarioDTO entityToDto(Usuario usuario);

	@Mapping(target = "email", source = "usuarioDTO.email")
	@Mapping(target = "password", source = "usuarioDTO.password")
	@Mapping(target = "nombres", source = "usuarioDTO.nombres")
	@Mapping(target = "apellidos", source = "usuarioDTO.apellidos")
	@Mapping(target = "direccion", source = "usuarioDTO.direccion")
	@Mapping(target = "telefono", source = "usuarioDTO.telefono")
	@Mapping(target = "foto", source = "usuarioDTO.foto")
	@Mapping(target = "usuarioCreacion", source = "usuarioCreacion")
	@Mapping(target = "fechaCreacion", source = "fechaCreacion")
	Usuario dtoToEntity(UsuarioDTO usuarioDTO, String usuarioCreacion, Date fechaCreacion);

}
