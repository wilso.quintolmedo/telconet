package com.woqg.telconet.repositoy;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.woqg.telconet.repositoy.entity.RefreshToken;
import com.woqg.telconet.repositoy.entity.Usuario;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {

	Optional<RefreshToken> findByToken(String token);
	Optional<RefreshToken> findByRefToken(String token);
	Optional<RefreshToken> findByUsuario(Usuario usuario);
	
}
