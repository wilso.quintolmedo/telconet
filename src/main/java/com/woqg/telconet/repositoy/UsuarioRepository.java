package com.woqg.telconet.repositoy;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.woqg.telconet.repositoy.entity.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	Optional<Usuario> findByEmail(String email);

	// @Transactional
	@Query(value = "CALL ENCONTRAR_USUARIO(:pv_usuario);", nativeQuery = true)
	List<Usuario> getUser(@Param("pv_usuario") String pvUsuario);
}
