package com.woqg.telconet.repositoy;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.woqg.telconet.repositoy.entity.UsuarioRol;

@Repository
public interface UsuarioRolRepository extends JpaRepository<UsuarioRol, Long> {

	@Modifying
	@Query(value = "delete from info_usuario_rol where rol_id = :rolId", nativeQuery = true)
	void deleteUsuarioRolByRol(@Param("rolId") Long rolId);
	
	@Modifying
	@Query(value = "delete from info_usuario_rol where usuario_id = :usuarioId", nativeQuery = true)
	void deleteUsuarioRolByUsuario(@Param("usuarioId") Long usuarioId);

}
