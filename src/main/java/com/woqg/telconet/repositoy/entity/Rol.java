package com.woqg.telconet.repositoy.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.woqg.telconet.bean.RolDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "admi_rol")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rol {

	@Id
	@Column(name = "rol_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nombre;
	private String descripcion;
	@OneToMany(mappedBy = "rol", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<UsuarioRol> roles;

	public Rol setValue(RolDTO rolDTO) {
		this.nombre = rolDTO.getNombre();
		this.descripcion = rolDTO.getDescripcion();
		return this;
	}

}
