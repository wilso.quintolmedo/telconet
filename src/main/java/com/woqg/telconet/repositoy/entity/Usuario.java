package com.woqg.telconet.repositoy.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.woqg.telconet.bean.UsuarioDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "admi_usuario")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Usuario {

	@Id
	@Column(name = "usuario_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "usuario")
	private String email;
	private String password;
	private String nombres;
	private String apellidos;
	private String direccion;
	private String telefono;
	private String foto;
	private String usuarioCreacion;
	@Column(insertable = false, updatable = false, nullable = true)
	private Date fechaCreacion;
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<UsuarioRol> roles;
	
	public Usuario setValue(UsuarioDTO usuarioDTO, String usuarioCreacion) {
		this.email = usuarioDTO.getDireccion();
		this.password = usuarioDTO.getPassword();
		this.nombres = usuarioDTO.getNombres();
		this.apellidos = usuarioDTO.getApellidos();
		this.direccion = usuarioDTO.getDireccion();
		this.telefono = usuarioDTO.getTelefono();
		this.foto = usuarioDTO.getFoto();
		this.usuarioCreacion = usuarioCreacion;
		return this;
	}
	
	

}
