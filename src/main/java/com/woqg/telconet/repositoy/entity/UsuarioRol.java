package com.woqg.telconet.repositoy.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "info_usuario_rol")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioRol {

	@Id
	@Column(name = "usuario_rol_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "usuario_id")
	@JsonBackReference
	private Usuario usuario;
	@ManyToOne
	@JoinColumn(name = "rol_id")
	@JsonBackReference
	private Rol rol;
	private String usuarioCreacion;
	@Column(insertable = false, updatable = false, nullable = true)
	private Date fechaCreacion;
	private String usuarioModificacion;
	@Column(insertable = false, updatable = false, nullable = true)
	private Date fechaModificacion;
	private String estado;

	public UsuarioRol(Usuario usuario, Rol rol, String nombreUsuario) {
		this.usuario = usuario;
		this.rol = rol;
		this.usuarioCreacion = nombreUsuario;
		this.usuarioModificacion = nombreUsuario;
		this.estado = "Activo";
	}

}
