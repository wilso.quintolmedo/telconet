package com.woqg.telconet.security.config;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.woqg.telconet.repositoy.entity.RefreshToken;
import com.woqg.telconet.security.service.DetallesUsuarioService;
import com.woqg.telconet.security.service.RefreshTokenService;
import com.woqg.telconet.util.JwtUtils;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class AuthenticationTokenFilter extends OncePerRequestFilter {

	@Autowired
	private JwtUtils jwtUtils;

	@Autowired
	RefreshTokenService refreshTokenSer;

	@Autowired
	private DetallesUsuarioService userDetailsService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			String jwt = jwtUtils.parseJwt(request.getHeader("Authorization"));
			if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
				Optional<RefreshToken> session = refreshTokenSer.findByToken(jwt);
				if (session.isPresent() && session.get().isRefreshActivo()) {
					String username = jwtUtils.getUserNameFromJwtToken(jwt);
					UserDetails userDetails = userDetailsService.loadUserByUsername(username);
					UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
							userDetails, null, userDetails.getAuthorities());
					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

					SecurityContextHolder.getContext().setAuthentication(authentication);
				}
			}
		} catch (Exception e) {
			log.error("Usuario no puede ser autenticado: {0}", e);
		}
		filterChain.doFilter(request, response);
	}

}
