package com.woqg.telconet.security.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.woqg.telconet.repositoy.UsuarioRepository;
import com.woqg.telconet.repositoy.entity.Usuario;
import com.woqg.telconet.security.bean.DetallesUsuario;

@Service
public class DetallesUsuarioService implements UserDetailsService {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Usuario> usuario = usuarioRepository.findByEmail(username);
		if (!usuario.isPresent()) {
			throw new UsernameNotFoundException("Usuario no encontrado");
		}
		return new DetallesUsuario(usuario.get());
	}
	
}