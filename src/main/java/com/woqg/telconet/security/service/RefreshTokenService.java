package com.woqg.telconet.security.service;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woqg.telconet.config.CustomException;
import com.woqg.telconet.repositoy.RefreshTokenRepository;
import com.woqg.telconet.repositoy.entity.RefreshToken;
import com.woqg.telconet.repositoy.entity.Usuario;

@Service
public class RefreshTokenService {

	@Autowired
	private RefreshTokenRepository refreshTokenRep;

	public Optional<RefreshToken> findByToken(String token) {
		return refreshTokenRep.findByToken(token);
	}
	
	public Optional<RefreshToken> findByRefToken(String token) {
		return refreshTokenRep.findByRefToken(token);
	}

	public RefreshToken createRefreshToken() {
		RefreshToken refreshToken = new RefreshToken();
		refreshToken.setFechaExpiracion(Instant.now().plusMillis(3600000));
		refreshToken.setRefToken(UUID.randomUUID().toString());
		refreshToken.setContarRefresh(0L);
		refreshToken.setRefreshActivo(true);
		return refreshToken;
	}

	public void verifyExpiration(RefreshToken token) throws CustomException {
		if (token.getFechaExpiracion().compareTo(Instant.now()) < 0) {
			throw new CustomException("Token de request expiro: " + token.getRefToken());
		}
	}

	public void verifyRefreshAvailability(RefreshToken refreshToken) throws CustomException {
		if (Boolean.FALSE.equals(refreshToken.isRefreshActivo())) {
			throw new CustomException("Refresh bloqueado para dispositivo: " + refreshToken.getRefToken());
		}
	}

	public RefreshToken save(RefreshToken refreshToken) {
		return refreshTokenRep.save(refreshToken);
	}
	
	public Optional<RefreshToken> buscarPorUsuario(Usuario usuario) {
		return refreshTokenRep.findByUsuario(usuario);
	}

}
