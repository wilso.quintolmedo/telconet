package com.woqg.telconet.security.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.woqg.telconet.bean.RolDTO;
import com.woqg.telconet.config.CustomException;
import com.woqg.telconet.mapper.MicroMapper;
import com.woqg.telconet.repositoy.RolRepository;
import com.woqg.telconet.repositoy.UsuarioRolRepository;
import com.woqg.telconet.repositoy.entity.Rol;
import com.woqg.telconet.util.Constantes;

@Service
public class RolService {

	@Autowired
	RolRepository rolRep;

	@Autowired
	UsuarioRolRepository usuarioRolRep;

	public List<RolDTO> obtener() {
		return rolRep.findAll().parallelStream().map(MicroMapper.INSTANCE::entityToDto).collect(Collectors.toList());
	}

	public RolDTO obtenerPorId(Long id) throws CustomException {
		Optional<Rol> rol = rolRep.findById(id);
		if (rol.isPresent())
			return MicroMapper.INSTANCE.entityToDto(rol.get());
		throw new CustomException("No se encontro registro");
	}

	public RolDTO crear(RolDTO rolDTO) {
		rolRep.save(MicroMapper.INSTANCE.dtoToEntity(rolDTO));
		return rolDTO;
	}

	public RolDTO actualizar(Long id, RolDTO rolDTO) throws CustomException {
		Optional<Rol> rol = rolRep.findById(id);
		if (rol.isPresent()) {
			rolRep.save(rol.get().setValue(rolDTO));
			return rolDTO;
		}
		throw new CustomException("No se encontro registro");
	}

	@Transactional(rollbackOn = Exception.class)
	public RolDTO eliminar(Long id) throws CustomException {
		Optional<Rol> rol = rolRep.findById(id);
		if (rol.isPresent()) {
			if (rol.get().getNombre().equals(Constantes.R_ADMIN))
				throw new CustomException(HttpStatus.CONFLICT, "No se puede eliminar rol ADMINISTRADOR");
			usuarioRolRep.deleteUsuarioRolByRol(id);
			rolRep.delete(rol.get());
			return MicroMapper.INSTANCE.entityToDto(rol.get());
		}
		throw new CustomException("No se encontro registro");
	}
}
