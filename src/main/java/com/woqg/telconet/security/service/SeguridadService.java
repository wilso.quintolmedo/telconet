package com.woqg.telconet.security.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.woqg.telconet.bean.LoginDTO;
import com.woqg.telconet.bean.LoginResponse;
import com.woqg.telconet.bean.TokenRefresh;
import com.woqg.telconet.config.CustomException;
import com.woqg.telconet.mapper.MicroMapper;
import com.woqg.telconet.repositoy.entity.RefreshToken;
import com.woqg.telconet.security.bean.DetallesUsuario;
import com.woqg.telconet.util.JwtUtils;

@Service
public class SeguridadService {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	RefreshTokenService refreshTokenSer;

	@Autowired
	JwtUtils jwtUtils;

	public LoginResponse autenticaUsuario(LoginDTO loginDTO) {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getEmail(), loginDTO.getClave()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String token = jwtUtils.generateJwtToken(authentication);
		DetallesUsuario detallesUsuario = (DetallesUsuario) authentication.getPrincipal();
		List<String> roles = detallesUsuario.getAuthorities().stream().map(GrantedAuthority::getAuthority)
				.collect(Collectors.toList());
		Optional<RefreshToken> refreshTokenOP = refreshTokenSer.buscarPorUsuario(detallesUsuario.getUsuario());
		RefreshToken refreshToken = refreshTokenSer.createRefreshToken();
		if (refreshTokenOP.isPresent()) {
			refreshToken = refreshTokenOP.get();
			refreshToken.setRefreshActivo(true);
		} else {
			refreshToken.setUsuario(detallesUsuario.getUsuario());
		}
		refreshToken.setToken(token);
		refreshToken = refreshTokenSer.save(refreshToken);
		return MicroMapper.INSTANCE.entityToDto(detallesUsuario.getUsuario(), roles, refreshToken,
				jwtUtils.getJwtExpiration());
	}

	public LoginResponse refreshJwtToken(TokenRefresh tokenRefresh) throws CustomException {
		Optional<RefreshToken> refreshTokenOp = refreshTokenSer.findByRefToken(tokenRefresh.getRefreshToken());
		if (refreshTokenOp.isPresent()) {
			RefreshToken refreshToken = refreshTokenOp.get();
			refreshTokenSer.verifyExpiration(refreshToken);
			refreshTokenSer.verifyRefreshAvailability(refreshToken);
			refreshToken.refresh(jwtUtils.generateTokenFromUser(refreshToken.getUsuario()));
			refreshTokenSer.save(refreshToken);
			return MicroMapper.INSTANCE.entityToDto(refreshToken, jwtUtils.getJwtExpiration());
		}
		throw new CustomException("Token no valido para refresh" + tokenRefresh.getRefreshToken());

	}

	public void logoutUsuario(String headerAuth) throws CustomException {
		String token = jwtUtils.parseJwt(headerAuth);
		if (token != null && jwtUtils.validateJwtToken(token)) {
			Optional<RefreshToken> session = refreshTokenSer.findByToken(token);
			if (session.isPresent() && session.get().isRefreshActivo()) {
				RefreshToken refreshToken = session.get();
				refreshToken.setRefreshActivo(false);
				refreshTokenSer.save(refreshToken);
			}
		} else
			throw new CustomException("Login de session no valido");
	}

}
