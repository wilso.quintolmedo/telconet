package com.woqg.telconet.security.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.woqg.telconet.bean.UsuarioDTO;
import com.woqg.telconet.config.CustomException;
import com.woqg.telconet.mapper.MicroMapper;
import com.woqg.telconet.repositoy.RolRepository;
import com.woqg.telconet.repositoy.UsuarioRepository;
import com.woqg.telconet.repositoy.UsuarioRolRepository;
import com.woqg.telconet.repositoy.entity.Rol;
import com.woqg.telconet.repositoy.entity.Usuario;
import com.woqg.telconet.repositoy.entity.UsuarioRol;

@Service
public class UsuarioService {

	@Autowired
	RolRepository rolRep;

	@Autowired
	UsuarioRepository usuarioRep;

	@Autowired
	UsuarioRolRepository usuarioRolRep;

	@Autowired
	PasswordEncoder encoder;

	public String usuarioLogin() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}
	
	public List<UsuarioDTO> obtenerEmail(String email) {
		return usuarioRep.getUser(email).parallelStream().map(MicroMapper.INSTANCE::entityToDto)
				.collect(Collectors.toList());
	}

	public List<UsuarioDTO> obtener() {
		return usuarioRep.findAll().parallelStream().map(MicroMapper.INSTANCE::entityToDto)
				.collect(Collectors.toList());
	}

	public UsuarioDTO obtenerPorId(Long id) throws CustomException {
		Optional<Usuario> usuario = usuarioRep.findById(id);
		if (usuario.isPresent())
			return MicroMapper.INSTANCE.entityToDto(usuario.get());
		throw new CustomException("No se encontro registro");
	}

	@Transactional(rollbackOn = Exception.class)
	public void crear(UsuarioDTO usuarioDTO) throws CustomException {
		Optional<Rol> rol = rolRep.findByNombre(usuarioDTO.getRol());
		if (!rol.isPresent()) {
			throw new CustomException("No se encontro el rol asignado");
		}
		String usuarioLogin = usuarioLogin();
		usuarioDTO.setPassword(encoder.encode(usuarioDTO.getPassword()));
		Usuario usuario = usuarioRep.save(MicroMapper.INSTANCE.dtoToEntity(usuarioDTO, usuarioLogin, new Date()));
		usuarioRolRep.save(new UsuarioRol(usuario, rol.get(), usuarioLogin));
	}

	@Transactional(rollbackOn = Exception.class)
	public UsuarioDTO actualizar(Long id, UsuarioDTO usuarioDTO) throws CustomException {
		Optional<Rol> rol = rolRep.findByNombre(usuarioDTO.getRol());
		if (!rol.isPresent()) {
			throw new CustomException("No se encontro el rol asignado");
		}
		Optional<Usuario> existen = usuarioRep.findByEmail(usuarioDTO.getEmail());

		if (existen.isPresent() && !existen.get().getId().equals(id)) {
			throw new CustomException("Nombre de usuario en uso");
		}
		String usuarioLogin = usuarioLogin();
		Optional<Usuario> usuarioActual = usuarioRep.findById(id);
		if (usuarioActual.isPresent()) {
			Usuario usuario = usuarioActual.get().setValue(usuarioDTO, usuarioLogin);
			usuario.setPassword(encoder.encode(usuarioDTO.getPassword()));
			usuarioRep.save(usuario);
			return usuarioDTO;
		}
		throw new CustomException("No se encontro registro");
	}

	@Transactional(rollbackOn = Exception.class)
	public UsuarioDTO eliminar(Long id) throws CustomException {
		Optional<Usuario> usuario = usuarioRep.findById(id);
		if (usuario.isPresent()) {
			usuarioRolRep.deleteUsuarioRolByUsuario(id);
			usuarioRep.delete(usuario.get());
			return MicroMapper.INSTANCE.entityToDto(usuario.get());
		}
		throw new CustomException("No se encontro registro");
	}

}
