package com.woqg.telconet.util;

public class Constantes {

	private Constantes() {
	    throw new IllegalStateException("Constantes class");
	  }

	public static final String R_CONSULTOR = "CONSULTOR";
	public static final String R_EDITOR = "EDITOR";
	public static final String R_ADMIN = "ADMINISTRADOR";
}
