INSERT INTO admi_rol (nombre, descripcion) VALUES ('CONSULTOR', 'CONSULTOR');;
INSERT INTO admi_rol (nombre, descripcion) VALUES ('EDITOR', 'EDITOR');;
INSERT INTO admi_rol (nombre, descripcion) VALUES ('ADMINISTRADOR', 'ADMINISTRADOR');;

INSERT INTO admi_usuario (usuario, password, nombres, apellidos, direccion, telefono, usuario_creacion) VALUES 
('administrador@prueba.com', '$2a$10$FOcBR6X/clb5I2Cq0q1Ke.5knyC91PJ/2Jw3r3mSO64uI6QCh7/hq', 'alex', 'smith', 'guayaquil', '48765234', 'SYS');;

INSERT INTO info_usuario_rol (usuario_id, rol_id, usuario_creacion, usuario_modificacion, estado) VALUES 
(1, 3, 'SYS', 'SYS', 'Activo');;
