DROP TABLE IF EXISTS info_usuario_rol;;
DROP TABLE IF EXISTS refresh_token;;
DROP TABLE IF EXISTS admi_rol;;
DROP TABLE IF EXISTS admi_usuario;;
DROP PROCEDURE IF EXISTS ENCONTRAR_USUARIO;;

CREATE TABLE admi_rol (
  rol_id INT AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(45) NOT NULL,
  descripcion VARCHAR(45) NOT NULL
);;

CREATE TABLE admi_usuario (
  usuario_id INT AUTO_INCREMENT PRIMARY KEY,
  usuario VARCHAR(45) UNIQUE NOT NULL,
  password VARCHAR(64) NOT NULL,
  nombres VARCHAR(100) NOT NULL,
  apellidos VARCHAR(100) NOT NULL,
  direccion VARCHAR(200) NOT NULL,
  telefono VARCHAR(200) NOT NULL,
  foto VARCHAR(300),
  usuario_creacion VARCHAR(45) NOT NULL,
  fecha_creacion timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL
);;
 
CREATE TABLE info_usuario_rol (
  usuario_rol_id INT AUTO_INCREMENT PRIMARY KEY,
  usuario_id INT NOT NULL,
  rol_id INT NOT NULL,
  usuario_creacion VARCHAR(45) NOT NULL,
  fecha_creacion timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  usuario_modificacion VARCHAR(45) NOT NULL,
  fecha_modificacion timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  estado VARCHAR(10) NOT NULL,
  CONSTRAINT info_rol_fk FOREIGN KEY (rol_id) REFERENCES admi_rol (rol_id),
  CONSTRAINT info_usuario_fk FOREIGN KEY (usuario_id) REFERENCES admi_usuario (usuario_id)
);;

CREATE TABLE refresh_token (
  refresh_token_id INT AUTO_INCREMENT PRIMARY KEY,
  usuario_id INT NOT NULL,
  token VARCHAR(256) NOT NULL,
  ref_token VARCHAR(256) NOT NULL,
  contar_refresh INT NOT NULL,
  fecha_expiracion timestamp NOT NULL,
  refresh_activo boolean NOT NULL,
  CONSTRAINT refresh_usuario_fk FOREIGN KEY (usuario_id) REFERENCES admi_usuario (usuario_id)
);;

CREATE PROCEDURE ENCONTRAR_USUARIO(IN pv_usuario VARCHAR(45))
BEGIN
SELECT * FROM admi_usuario WHERE usuario = pv_usuario;
END ;;
